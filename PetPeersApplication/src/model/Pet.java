package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pet")
public class Pet {
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pet_id;
	@Column
	private String pet_categ;
	@Column
	private String breed;
	@Column
	private float weight;
	@Column
	private float height;
	@Column
	private int age;
	@Column
	private String colour;
	@Column
	private float cost;
	@Column
	private String user;

	public Pet(int pet_id, String pet_categ, String breed, float weight, float height, int age, String colour,
			float cost, String user) {
		this.pet_id = pet_id;
		this.pet_categ = pet_categ;
		this.breed = breed;
		this.weight = weight;
		this.height = height;
		this.age = age;
		this.colour = colour;
		this.cost = cost;
		this.user = user;
	}

	public Pet() {
		// TODO Auto-generated constructor stub
	}

	public int getPet_id() {
		return pet_id;
	}

	public void setPet_id(int pet_id) {
		this.pet_id = pet_id;
	}

	public String getPet_categ() {
		return pet_categ;
	}

	public void setPet_categ(String pet_categ) {
		this.pet_categ = pet_categ;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	// @Override
//	public String toString() {
//		return "Pet [pet_id=" + pet_id + ", pet_categ=" + pet_categ + ", breed=" + breed + ", weight=" + weight
//				+ ", height=" + height + ", age=" + age + ", colour=" + colour + ", cost=" + cost + ", user=" + user
//				+ "]";
//	}

}