<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Petpeers Application
        </title>
<link rel="stylesheet" type="text/css" href="resources/css/Home_style.css"/>
    </head>
    <body>
        <div class="topnav">
            <a class="active"><img src="resources/img/paw.png" width="50px" height="50px"></a>
            <a>Pets Management</a>
            <div class="topnav-right">
              <a href="logout">Logout</a>
            </div>
          </div>
     <div class="screen">
     <form:form>
          <br/>
            <button class="button button1"  type="submit" formaction="viewallpets">View All Pets</button>
            <button class="button button2"  type="submit" formaction="addpet">Add a Pet</button>
            <button class="button button3"  type="submit" formaction="viewMypets">View My Pets</button>       
     </form:form> 
    </div>
         	<div id="footer">
		 <p>&copy;Designed by Omprasad</p>
	</div>
    </body>
</html>